#define _CRT_SECURE_NO_WARNINGS

#include "RelicTextureManager.h"
#include "RelicMap/RelicMap.h"
#include "RelicFileLoad.h"

//Texture managger

//Loads a texture into the memory
RelicTexture_t *LoadTexture(RelicTextureManager_t *slef, char *name) {
    //TODO: make sure it is freed up
    RelicTexture_t *texture;

    texture = ParseSheetFile(name);

    char base[255] = "Textures\\";
    strcat(base, texture->source);

    texture->texture = IMG_LoadTexture(g_renderer->sdlRenderer, base);

    if (texture->texture == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_DEBUG,"ERROR: MEM: Can't load the image: %s", IMG_GetError());
        exit(-1);
    }

    //texture->source = malloc(strlen(name) * sizeof(char) + 1);
    //strcpy(texture->source, name);

    return texture;
}

RelicTexture_t *GetTexture(RelicTextureManager_t *self, char *name) {
    for (int j = 0; j <= self->texturesCount; j++) {
        if (strcmp(self->textures[j]->name, name)==0) {
            return self->textures[j];
        }
    }

    self->texturesCount += 1;
    self->textures[self->texturesCount] = LoadTexture(self, name);

    return self->textures[self->texturesCount];
}

RelicTextureManager_t *Create_TextureManager() {

    RelicTextureManager_t *manager = malloc(sizeof(RelicTextureManager_t));
    if (manager == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (RelicTextureManagger)");
        exit(-1);
    }

    manager->texturesCount = -1;

    return manager;

}

void DestroySprite(struct Sprite self){
    if(self.hasCollision) {
        free(self.collision.vertices);
    }
    if (self.frameCount > 0) {
        free(self.frames);
    }

}

void DestroyTexture(RelicTexture_t *self) {
    //free(self->source);
    for (int i = 0; i < self->rows * self->colums; ++i) {
        DestroySprite(self->sprites[i]);
    }

    free(self->sprites);

    SDL_free(self->texture);
    free(self);
}

void FreeTextureManager(RelicTextureManager_t *self) {
    for (int i = 0; i <= self->texturesCount; ++i) {
        DestroyTexture(self->textures[i]);
    }
    free(self);
}

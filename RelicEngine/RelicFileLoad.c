#include <RelicEngine/RelicEntitys/RelicEntity.h>
#include "RelicFileLoad.h"

#include "lib/debugmalloc.h"

#include "Shared.h"
#include "RelicTextureManager.h"
#include <RelicEntitys/RelicEntity.h>

#ifndef INT_MAX
#define INT_MAX INT32_MAX
#endif

//Forvard dec.
void m_readSheetData(char *buffer, FILE *fp_map, RelicMap_t *rMap);

void m_readLayerData(char *buffer, FILE *fp_map, RelicMap_t *rMap);

void m_readEntitiesData(char *buffer, FILE *fp_map, RelicMap_t *rMap);


void s_version_1_0_0(FILE *fp, RelicTexture_t *res);

void s_version_2_0_0(FILE *fp, RelicTexture_t *res);

void s_animationData(char *buffer, FILE *fp, RelicTexture_t *res);

void s_collisionData(char *buffer, FILE *fp, RelicTexture_t *res);


//void p_readActionData(char *buffer, FILE *fp, RelicPlayer_t *player);

void c_readActionData(char *buffer, FILE *fp, CharacterData *data);
void c_readPropertyData(char *buffer, FILE *fp, CharacterData *data);


//Unitil
struct Version {
    int MAJOR;
    int MINOR;
    int PATCH;
};

struct Version OpenFile(char *name, char *base, char *ext, char *format, FILE **fp) {

    char *result = malloc(strlen(base) + strlen(name) + strlen(ext) + 1);
    strcpy(result, base);
    strcat(result, name);
    strcat(result, ext);

    *fp = fopen(result, "r");
    if (*fp == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Can't open \" %s \"", result);
        exit(10);
    }
    //we no longer need the string so we can free it up
    free(result);

    //Create the header pattern
    result = malloc(strlen(ext) + strlen("-%d.%d.%d\n") + 1);
    strcpy(result, format);
    strcat(result, "-%d.%d.%d\n");

    char *buffer = malloc(sizeof(char) * 10000);
    fgets(buffer, sizeof(char) * 10000, *fp);

    struct Version v = {-1, -1, -1};

    //Check for the file header
    //int MAJOR = -1, MINOR = -1, PATCH = -1;
    if (sscanf(buffer, result, &v.MAJOR, &v.MINOR, &v.PATCH) == 3) {
        //no need for the pattern anymore

    } else {
        SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "%s is not %s format", name, ext);
        fclose(*fp);
    }

    free(result);
    free(buffer);
    return v;
}


//Sheet files
void ParseSheetFile_ref(char *name, RelicTexture_t *res) {
    //Open the file
    FILE *fp = NULL;

    struct Version v = OpenFile(name, "Textures/", ".rsf", "rsf", &fp);
    //region Version 1
    if (v.MAJOR == 1) {

        s_version_1_0_0(fp, res);
    }
        //endregion
    else if (v.MAJOR == 2) {

        s_version_2_0_0(fp, res);
    }
}

void s_version_1_0_0(FILE *fp, RelicTexture_t *res) {

    //Name of the tile sheet
    fscanf(fp, "%s", &res->name);
    //Source of the file
    fscanf(fp, "%s", &res->source);
    //Discard the line
    fscanf(fp, "%*s");
    //load the columns and rows
    fscanf(fp, "%d,%d", &res->colums, &res->rows);
    //load the size of the tiles
    fscanf(fp, "%d,%d", &res->xSize, &res->ySize);

    //Create the Sprites for the atlas
    //TODO: Free up in a destroy
    res->sprites = malloc(sizeof(Sprite_t) * res->rows * res->colums);
    if (res->sprites == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (Sprite)");
        exit(-1);
    }

    for (int x = 0; x < res->colums; x++) {
        for (int y = 0; y < res->rows; y++) {
            int id = y * res->colums + x;
            res->sprites[id].x = x * res->xSize;
            res->sprites[id].y = y * res->ySize;
            res->sprites[id].width = res->ySize;
            res->sprites[id].height = res->xSize;
            res->sprites[id].texture = res;

            res->sprites[id].currentFrame = -1;
            res->sprites[id].frameCount = 0;
            res->sprites[id].lastTime = 0;
        }
    }


    //Load the anim data for the sprites
    int CurrAnimID = 0;
    int FrameCount = 0;
    char remaining[255];

    //parse the anim data lines
    while (fscanf(fp, "%d-%d,%s", &CurrAnimID, &FrameCount, remaining) == 3) {

        //TODO free up in a destroy
        int sizeToAlloc = sizeof(AnimFrame) * FrameCount;

        res->sprites[CurrAnimID].currentFrame = 0;
        res->sprites[CurrAnimID].lastTime = 0;
        res->sprites[CurrAnimID].frameCount = FrameCount;

        res->sprites[CurrAnimID].frames = (AnimFrame *) malloc(sizeToAlloc);
        if (res->sprites[CurrAnimID].frames == NULL) {
            SDL_Log("ERROR: MEM: Can't allocate memory (AnimFrame)");
            exit(-1);
        }


        int j = 0;
        int id = 0;
        int duration = 0;
        while (sscanf(remaining, "%d:%d,%s", &id, &duration, remaining) == 3) {
            res->sprites[CurrAnimID].frames[j].GID = id;
            res->sprites[CurrAnimID].frames[j].time = duration;
            res->sprites[CurrAnimID].frames[j].sprite = &(res->sprites[id]);

            j++;
        }
        sscanf(remaining, "%d:%d,%s", &id, &duration, remaining);
        res->sprites[CurrAnimID].frames[j].GID = id - 1;
        res->sprites[CurrAnimID].frames[j].time = duration;
        res->sprites[CurrAnimID].frames[j].sprite = &(res->sprites[id]);

        j++;

    }
}

void s_version_2_0_0(FILE *fp, RelicTexture_t *res) {

    //Name of the tile sheet
    fscanf(fp, "%s", res->name);
    //Source of the file
    fscanf(fp, "%s", res->source);
    //Discard the line
    fscanf(fp, "%*s");
    //load the columns and rows
    fscanf(fp, "%d,%d", &res->colums, &res->rows);
    //load the size of the tiles
    fscanf(fp, "%d,%d", &res->xSize, &res->ySize);

    //Create the Sprites for the atlas
    //TODO: Free up in a destroy
    //TODO: separate function
    res->sprites = malloc(sizeof(Sprite_t) * res->rows * res->colums);
    if (res->sprites == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (Sprite)");
        exit(-1);
    }

    for (int x = 0; x < res->colums; x++) {
        for (int y = 0; y < res->rows; y++) {
            int id = y * res->colums + x;
            res->sprites[id].x = x * res->xSize;
            res->sprites[id].y = y * res->ySize;
            res->sprites[id].width = res->xSize;
            res->sprites[id].height = res->ySize;
            res->sprites[id].texture = res;

            res->sprites[id].currentFrame = -1;
            res->sprites[id].frameCount = 0;
            res->sprites[id].lastTime = 0;
            res->sprites[id].frames = NULL;

            res->sprites[id].hasCollision = false;
            res->sprites[id].collision.vertCount = 0;
            res->sprites[id].collision.vertices = NULL;
        }
    }

    char *buffer = malloc(sizeof(char) * 10000);
    while (fgets(buffer, sizeof(char) * 10000, fp) != NULL) {
        if (buffer[0] == '/') {
            switch (buffer[1]) {
                case 'A':
                    s_animationData(buffer, fp, res);
                    break;
                case 'C':
                    s_collisionData(buffer, fp, res);
                    break;
                default:
                    break;
            }
        }
    }
    if (feof(fp)) {
        // hit end of file
        fclose(fp);
    } else {
        // some other error interrupted the read
        SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Unknown error in reading the file");
        fclose(fp);
    }
    free(buffer);



}

void s_animationData(char *buffer, FILE *fp, RelicTexture_t *res) {
    int j = 0;

    int CurrAnimID = 0;
    int FrameCount = 0;
    char remaining[255];

    if (sscanf(buffer, "/A-%d-%d,%s", &CurrAnimID, &FrameCount, remaining) > 0) {

        res->sprites[CurrAnimID].currentFrame = 0;
        res->sprites[CurrAnimID].lastTime = 0;
        res->sprites[CurrAnimID].frameCount = FrameCount;


        //TODO free up in a destroy
        res->sprites[CurrAnimID].frames = (AnimFrame *) malloc(sizeof(AnimFrame) * FrameCount);
        if (res->sprites[CurrAnimID].frames == NULL) {
            SDL_Log("ERROR: MEM: Can't allocate memory (AnimFrame)");
            exit(10);
        }


        int j = 0;
        int id = 0;
        int duration = 0;
        while (sscanf(remaining, "%d:%d,%s", &id, &duration, remaining) == 3) {
            res->sprites[CurrAnimID].frames[j].GID = id;
            res->sprites[CurrAnimID].frames[j].time = duration;
            res->sprites[CurrAnimID].frames[j].sprite = &(res->sprites[id]);

            j++;
        }
        sscanf(remaining, "%d:%d,%s", &id, &duration, remaining);
        res->sprites[CurrAnimID].frames[j].GID = id;
        res->sprites[CurrAnimID].frames[j].time = duration;
        res->sprites[CurrAnimID].frames[j].sprite = &(res->sprites[id]);

        //j++;

    }
}

void s_collisionData(char *buffer, FILE *fp, RelicTexture_t *res) {

    int CurrID = 0;
    int VertexCount = 0;
    char remaining[255] = {0};

    if (sscanf(buffer, "/C-%d-%d,%s\n", &CurrID, &VertexCount, remaining) > 0) {
        Shape *shape = malloc(sizeof(Shape));
        shape->vertices = malloc(sizeof(Vector2double_t) * VertexCount);
        shape->vertCount = VertexCount;
        int i = 0;

        while (sscanf(remaining, "%lf:%lf,%s\n", &shape->vertices[i].x, &shape->vertices[i].y, remaining) == 3) {
            i++;
        }
        sscanf(remaining, "%lf:%lf\n", &shape->vertices[i].x, &shape->vertices[i].y);

        res->sprites[CurrID].collision = *shape;
        res->sprites[CurrID].hasCollision = 1;

        //free(shape->vertices);
        free(shape);

    }

}

RelicTexture_t *ParseSheetFile(char *name) {

    //Allocate the memory for the map
    RelicTexture_t *res = malloc(sizeof(RelicTexture_t));
    if (res == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (RelicMapTileSheet)");
        exit(-1);
    }

    ParseSheetFile_ref(name, res);

    return res;
}


//Map files
RelicMap_t *ParseMapFile(char *filename) {

    FILE *fp_map = NULL;
    OpenFile(filename, "Maps/", ".rmf", "rmf", &fp_map);

    //Allocate the memory for the map
    RelicMap_t *rMap = malloc(sizeof(RelicMap_t));
    if (rMap == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (RelicMap)");
        exit(-1);
    }
    rMap->entities = RelicListCreateList();

    fscanf(fp_map, "%s",rMap->name);

    fscanf(fp_map, "%d,%d", &rMap->width, &rMap->height);
    fscanf(fp_map, "%d,%d", &rMap->tileWidth, &rMap->tileHeight);

    char *buffer = malloc(sizeof(char) * 10000);
    while (fgets(buffer, sizeof(char) * 10000, fp_map) != NULL) {
        if (buffer[0] == '/') {
            switch (buffer[1]) {
                case 'S':
                    m_readSheetData(buffer, fp_map, rMap);
                    break;
                case 'L':
                    m_readLayerData(buffer, fp_map, rMap);
                    break;
                case 'E':
                    m_readEntitiesData(buffer, fp_map, rMap);
                    break;
                default:
                    break;
            }
        }
    }
    if (feof(fp_map)) {
        // hit end of file
        fclose(fp_map);
    } else {
        // some other error interrupted the read
        SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Unknown error in reading the file");
        fclose(fp_map);
    }
    free(buffer);


    rMap->defaultLight = 200;
    return rMap;
}

void m_readSheetData(char *buffer, FILE *fp_map, RelicMap_t *rMap) {

    //A static variable to keep the indexer between the calls
    static int i = 0;

	static RelicMap_t *tracker;
	if(tracker != rMap) {
		i = 0;
		tracker = rMap;
	}

    if (sscanf(buffer, "/SC-%d", &rMap->sheetsCount) > 0) {
        rMap->sheets = malloc(sizeof(RelicMapTileSheet_t) * (rMap->sheetsCount+1));
        return;
    } else if (sscanf(buffer, "/S-%[^,],%d", rMap->sheets[i].source, &(rMap->sheets[i].startID))) {

        rMap->sheets[i].texture = GetTexture(g_textureManager,rMap->sheets[i].source);
        //SDL_LogError(RELICLOG_CATEGORY_DEBUG,"MapParse: %s : %d",rMap->sheets[i].source,rMap->sheets[i].startID);

        //Increment the index for the next load call
        i++;

        rMap->sheets[i].startID = INT_MAX;
    }else {
        SDL_LogError(RELICLOG_CATEGORY_ERROR,"[FILE]: Can't parse line: %s",buffer);
    }

}

void m_readLayerData(char *buffer, FILE *fp_map, RelicMap_t *rMap) {

    //A static variable to keep the indexer between the calls
    static int i = 0;

	static RelicMap_t *tracker;
	if(tracker != rMap) {
		i = 0;
		tracker = rMap;
	}

	char visible;

    if (sscanf(buffer, "/LC-%d", &rMap->layerLength) > 0) {
        //Allocate the layer structs
        rMap->layers = malloc(sizeof(RelicMapLayer_t) * (rMap->layerLength));
        if (rMap->layers == NULL) {
            SDL_LogError(RELICLOG_CATEGORY_MEMORY_ALLOC, "ERROR: MEM: Can't allocate memory (RelicMapLayer)");
            exit(-1);
        }
        rMap->layerCount = 0;
        return;
    } else if (sscanf(buffer, "/L-%[^,:]:%c", rMap->layers[rMap->layerCount].name,&visible)) {

        //Allocate the map array
        //TODO: this should be 2d array not a flattened out one
        rMap->layers[rMap->layerCount].map = malloc(sizeof(int) * rMap->width * rMap->height);
        if (rMap->layers[rMap->layerCount].map == NULL) {
            SDL_LogError(RELICLOG_CATEGORY_MEMORY_ALLOC, "ERROR: MEM: Can't allocate memory (Map array)");
            exit(-1);
        }

        for (int y = 0; y < rMap->height; y++) {
            fgets(buffer, sizeof(char) * 10000, fp_map);

            for (int x = 0; x < rMap->width; x++) {
                sscanf(buffer, "%d,%s", GetMapTile(rMap, rMap->layerCount, (Vector2int_t) {.x=x, .y=y}), buffer);

            }

        }

        if(visible == 'H')
            rMap->layers[rMap->layerCount].visible = false;
        else if(visible == 'V')
            rMap->layers[rMap->layerCount].visible = true;

        //i++;
        rMap->layerCount ++;
    }

}


void FreeValuKeyList( ValueKey *first){
    ValueKey *temp = first;
    while (temp != NULL) {
        ValueKey *next = temp->next;
        free(temp);
        temp = next;
    }
}

void m_readEntitiesData(char *buffer, FILE *fp_map, RelicMap_t *rMap) {

    //A static variable to keep the indexer between the calls
    static int i = 0;

    static RelicMap_t *tracker;
    if(tracker != rMap) {
		i = 0;
		tracker = rMap;
	}

    int count = 0;

    if (sscanf(buffer, "/E-%*[^,],%d", &count)) {

        for (int x = 0; x < count; x++) {
            fgets(buffer, sizeof(char) * 10000, fp_map);

            int id;
            Vector2double_t pos;
            char type[20];
            char name[20];

            char*dataBuffer = malloc(sizeof(char)*1000);
            dataBuffer[0] = 0;

            sscanf(buffer, "%d-%lf:%lf-%[^:]:%[^;\n];%s", &id, &pos.x, &pos.y, name, type,dataBuffer);
            ValueKey *data = NULL;

            if(strcmp(dataBuffer, "")!=0) {

                data = malloc(sizeof(ValueKey));
                ValueKey *current = data;
                while (sscanf(dataBuffer, "%[^:]:%[^;];%s", current->key, current->value, dataBuffer) == 3) {
                    current->next = malloc(sizeof(ValueKey));
                    current = current->next;
                }
                sscanf(dataBuffer,"%[^:]:%[^-]", current->key, current->value);
                current->next=NULL;

            }
            RelicEntity_t *e = CreateEntityOfType(type,pos,name,data);
            if(e != NULL) {
                AddEntity(rMap, e);
            }

            FreeValuKeyList(data);

            free(dataBuffer);
        }


        i++;

    }

}

/*
//Player Files
void ParsePlayerFile(char *name, RelicPlayer_t *player) {
    FILE *fp;
    struct Version v = OpenFile(name, "./", ".rpf", "rpf", &fp);
    if (v.MAJOR == 1) {

        fscanf(fp, "%s\n", player->name);

        char sheet[32];
        fscanf(fp, "%s", sheet);
        player->player_sheet = GetTexture(g_textureManager,sheet);

        char *buffer = malloc(sizeof(char) * 10000);
        while (fgets(buffer, sizeof(char) * 10000, fp) != NULL) {
            if (buffer[0] == '/') {
                switch (buffer[1]) {
                    case 'A':
                        p_readActionData(buffer, fp, player);
                        break;
                    default:
                        break;
                }
            }
        }
        if (feof(fp)) {
            // hit end of file
            fclose(fp);
        } else {
            // some other error interrupted the read
            SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Unknown error in reading the file");
            fclose(fp);
        }
        free(buffer);
    }

}

void p_readActionData(char *buffer, FILE *fp, RelicPlayer_t *player) {

    //A static variable to keep the indexer between the calls
    static int i = 0;

	static RelicPlayer_t *tracker;
	if(tracker != player) {
		i = 0;
		tracker = player;
	}

    if (sscanf(buffer, "/AC-%d", &player->actionCount) > 0) {
        player->actions = malloc(sizeof(char) * 20 * (player->actionCount));
        player->actionSprites = malloc(sizeof(int) * (player->actionCount));
        return;
    } else if (sscanf(buffer, "/A-%[^:]:%d", player->actions[i], &player->actionSprites[i])) {
        //Increment the index for the next load call
        i++;
    }
}
*/
//Character Files

CharacterData* ParseCharacterFile(char *name) {
    CharacterData *data = NULL;

    FILE *fp;
    struct Version v = OpenFile(name, "./", ".rcf", "rcf", &fp);
    if (v.MAJOR == 1) {

        data = malloc(sizeof(CharacterData));
        data->propertyes = NULL;

        fscanf(fp, "%s\n", data->name);

        char sheet[32];
        fscanf(fp, "%s", sheet);
        data->characterSheat = GetTexture(g_textureManager,sheet);

        char *buffer = malloc(sizeof(char) * 10000);
        while (fgets(buffer, sizeof(char) * 10000, fp) != NULL) {
            if (buffer[0] == '/') {
                switch (buffer[1]) {
                    case 'A':
                        c_readActionData(buffer, fp, data);
                        break;
                    case 'P':
                        c_readPropertyData(buffer, fp, data);
                        break;
                    default:
                        break;
                }
            }
        }
        if (feof(fp)) {
            // hit end of file
            fclose(fp);
        } else {
            // some other error interrupted the read
            SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Unknown error in reading the file");
            fclose(fp);
        }
        free(buffer);
    }

    return data;

}

void c_readActionData(char *buffer, FILE *fp, CharacterData *data) {

    //A static variable to keep the indexer between the calls
    static int i = 0;

    static CharacterData *tracker;
    if(tracker != data) {
        i = 0;
        tracker = data;
    }

    if (sscanf(buffer, "/AC-%d", &data->actionCount) > 0) {
        data->actions = malloc(sizeof(Action) * (data->actionCount));
        return;
    } else if (sscanf(buffer, "/A-%[^:]:%d", data->actions[i].name, &data->actions[i].strite)) {
        //Increment the index for the next load call
        i++;
    }
}

void c_readPropertyData(char *buffer, FILE *fp, CharacterData *data) {

    char name[32];
    char value[32];
    if (sscanf(buffer, "/P-%[^:]:%s", name, value)) {
        //Increment the index for the next load call
        ValueKey *newProp =malloc(sizeof(ValueKey));
        strcpy(newProp->value, value);
        strcpy(newProp->key, name);
        newProp->next = NULL;
        if(data->propertyes == NULL){
            data->propertyes = newProp;
        }
        else
        {
            data->propertyes->next = newProp;
        }
    }
}
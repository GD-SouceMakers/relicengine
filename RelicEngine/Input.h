#ifndef RELIC_INPUT_H_
#define RELIC_INPUT_H_

#include "stdbool.h"

typedef struct Input {
	int forward;
	int left;
	int esc;

	int mouse_x;
	int mouse_y;
	bool mouse_hold;
} Input;

Input globalInput;

#endif // !RELIC_INPUT_H_

#include <RelicEngine/RelicTextureManager.h>
#include <Program.h>
#include "RelicMapManager.h"
//#include "RelicMap.h"

#include "RelicFileLoad.h"

RelicMapManager_t *g_mapManager;


void CreateGameCamera() {

    RelicEntity_t *player = GetEntityByName(g_mapManager->activeMap, "MainCamera");
    if (player == NULL) {
        RelicEntity_t *e = CreateEntityOfType("camera", (Vector2double_t) {0, 0}, "MainCamera", NULL);
        AddEntity(g_mapManager->activeMap, e);
    }


}

void CreateDefaultPlayer(char *destName) {
    Vector2double_t playerPos;


    RelicEntity_t *entity;
    if (destName == NULL) {
        entity = GetEntityByType(g_mapManager->activeMap, "info_player_start");
    } else {
        entity = GetEntityByName(g_mapManager->activeMap, destName);
    }
    if (entity == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_MAP, "No %s in the map", destName);
        playerPos.x = 2;
        playerPos.y = 2;
    } else {
        playerPos.x = entity->pos.x;
        playerPos.y = entity->pos.y;
    }

    RelicEntity_t *player = GetEntityByName(g_mapManager->activeMap, "MainPlayer");
    if (player == NULL) {
        RelicEntity_t *e = CreateEntityOfType("player", playerPos, "MainPlayer", NULL);
        AddEntity(g_mapManager->activeMap, e);
    } else {
        player->pos = playerPos;
    }

}

RelicMap_t *ParseMap(RelicMapManager_t *self, char *name) {
    RelicMap_t *map = ParseMapFile(name);
    if (map == NULL) {
        SDL_LogError(RELICLOG_CATEGORY_FILE_LOAD, "Couldn't load map");
        exit(-1);
    }

    //LoadNeededTextures(g_textureManager, map);

    //self->activeMap = map;

    return map;
}

void LoadMap(RelicMapManager_t *self, char *name, char *destName) {

    self->nextMapName = malloc(sizeof(char) * (strlen(name) + 1));
    strcpy(self->nextMapName, name);
    if (destName != NULL) {
        self->nextMapObject = malloc(sizeof(char) * (strlen(name) + 1));
        strcpy(self->nextMapObject, destName);
    } else {
        self->nextMapObject = NULL;
    }
    g_relicapp->state = NEW_MAP;
}

void DoMapLoad(RelicMapManager_t *self) {
    RelicMap_t *map = ParseMap(self, self->nextMapName);


    SDL_LogError(RELICLOG_CATEGORY_DEBUG,"MapLoad1: %s : %s : %d",map->name,map->sheets[0].source,0);


    if (self->activeMap != NULL) {

        for (int i = 0; i < self->activeMap->entities->count; ++i) {
            RelicEntity_t *entity = RelicListGetElement(self->activeMap->entities, i);
            if (entity->doNotDestroyOnLoad) {
                AddEntity(map, entity);
            }
        }

        FreeMap(self->activeMap, false);
    }

    self->activeMap = map;
    CreateGameCamera();
    CreateDefaultPlayer(self->nextMapObject);

    free(self->nextMapName);
    free(self->nextMapObject);

}

bool unload = false;

void UnloadMap(RelicMapManager_t *self) {
    if (self->activeMap != NULL) {
        FreeMap(self->activeMap, true);
        self->activeMap = NULL;
    }
}


void DrawActiveMap(RelicMapManager_t *self) {
    self->activeMap->DrawMap(self->activeMap);
}

RelicMapManager_t *Create_MapManager() {
    RelicMapManager_t *manager = malloc(sizeof(RelicMapManager_t));
    if (manager == NULL) {
        SDL_Log("ERROR: MEM: Can't allocate memory (RelicMapManager)");
        exit(-1);
    }
    manager->activeMap = NULL;

    return manager;
}

void FreeMapManager() {
    FreeMap(g_mapManager->activeMap, true);
    free(g_mapManager);
}
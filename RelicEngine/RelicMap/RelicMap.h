#ifndef RELIC_MAP_H
#define RELIC_MAP_H

#include <RelicEngine/RelicList.h>
#include "Shared.h"
#include "RelicEngine/RelicEntitys/RelicEntity.h"
//#include "../RelicTextureManager.h"


#include "../RelicRenderer.h"
#include "RelicPhysics.h"


//struct RelicTexture;

typedef struct RelicMapTileSheet {
	char source[255];

	int startID;

	struct RelicTexture *texture;
} RelicMapTileSheet_t;

typedef struct RelicMapLayer
{
	int(*map); //a row mayor flat 2d array
	char name[20];
	bool visible;
} RelicMapLayer_t;

typedef struct RelicMap
{
	char name[32];

	//Map
	RelicMapLayer_t *layers;
	int layerCount;
	int layerLength;

	RelicList *entities;


	RelicMapTileSheet_t *sheets;
	int sheetsCount;

	int height;
	int width;

	int tileHeight;
	int tileWidth;

	int defaultLight;

	void (*DrawMap)(struct RelicMap *self);
} RelicMap_t;

void DrawMap(RelicMap_t *self);

RelicEntity_t* GetEntityByName(RelicMap_t *self, char *name);
RelicEntity_t* GetEntityByID(RelicMap_t *self, int id);
RelicEntity_t* GetEntityByType(RelicMap_t *self, char *type);

int* GetMapTile(RelicMap_t *self,int layer, Vector2int_t pos);
Sprite_t * GetMapTileSprite(RelicMap_t *self, int layer, Vector2int_t pos);
void Vector2int_to_tileWorldCord(RelicMap_t *self, Vector2int_t *a);
void Vector2int_to_tileCord(RelicMap_t *self, Vector2int_t *a);
void Vector2int_to_WorldCord(RelicMap_t *self, Vector2int_t *a);

///Adds the entity to the entity list of the map
///Assignis the id value of the entity
void AddEntity(RelicMap_t *self, RelicEntity_t *entity);
void DestroyEntity(RelicMap_t *self, RelicEntity_t *entity);


void StartAllEntities(RelicMap_t *self);
void UpdateAllEntities(RelicMap_t *self);
void LateUpdateAllEntities(RelicMap_t *self);
void PhysicsUpdateAllEntities(RelicMap_t *self);


void FreeMap(RelicMap_t *self, bool destroyAll);

#endif // !RELIC_MAP_H


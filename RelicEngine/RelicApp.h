#ifndef RELIC_APP_H
#define RELIC_APP_H

#include "Shared.h"

#include "RelicRenderer.h"
#include "RelicMap/RelicMapManager.h"

enum RelicApp_State{
	IN_GAME,
	NEW_MAP,
	MAP_TRANSITION,
	END_GAME
};


typedef struct RelicApp
{
	SDL_Window *window;
	RelicRenderer *renderer;


	int state;

	//struct RelicMap *map;
} RelicApp;

void InitApp(RelicApp *self);

void StartApp(RelicApp *self);

void StartGame();
void EndGame();

extern RelicApp *g_relicapp;

#endif // !RELIC_APP_H



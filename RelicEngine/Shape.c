//
// Created by dpete on 11/2/2018.
//

#include "lib/debugmalloc.h"
#include "Shape.h"

Shape *CreateRect(int width,int height){
    Shape *res = malloc(sizeof(Shape));
    res->vertCount = 4;
    res->vertices = malloc(sizeof(Vector2double_t)*4);
    double x = width/2;
    double y = height/2;
    res->vertices[0] = (Vector2double_t){-x,-y};
    res->vertices[1] = (Vector2double_t){x,-y};
    res->vertices[2] = (Vector2double_t){x,y};
    res->vertices[3] = (Vector2double_t){-x,y};
    return res;
}

void TranslateShape(Shape *a, Vector2double_t vec) {
    for (int i = 0; i < a->vertCount; ++i) {
        Vector2double_Add(&a->vertices[i], vec);
    }
}

Vector2double_t ShapeCenter(Shape s){
    double cx = 0;
    double cy = 0;

    for (int i = 0; i < s.vertCount; ++i) {
        cx += s.vertices[i].x;
        cy += s.vertices[i].y;
    }

    cx/=s.vertCount;
    cy/=s.vertCount;

    return (Vector2double_t){cx,cy};
}

Shape *ShapeDuplicate(Shape s) {
    Shape *res = malloc(sizeof(Shape));

    res->vertCount = s.vertCount;

    res->vertices = malloc(sizeof(Vector2double_t)*s.vertCount);
    for (int i = 0; i < res->vertCount; ++i) {
        res->vertices[i] = s.vertices[i];
    }

    return res;
}

void FreeShape(Shape *s){
    free(s->vertices);
    free(s);
}
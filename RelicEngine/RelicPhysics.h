#ifndef RELICENGINE_RELICPHYSICS_H
#define RELICENGINE_RELICPHYSICS_H


#include "Shared.h"
#include "Vector2int.h"
#include "RelicTimer.h"
#include "RelicMapManager.h"

#include "RelicRenderer.h"

struct RelicEntity;

typedef struct Projection {
    double min;
    double max;
} Projection;

typedef struct Physics{
    Shape *shape;

    bool isStatic;
    Vector2double_t velocity;
    Vector2double_t *pos; //Pointer to the Entities position

    struct RelicEntity *collision;

} Physics;

void PhysicsUpdate(Physics *obj);

#endif //RELICENGINE_RELICPHYSICS_H

//
// Created by dpete on 11/16/2018.
//

#ifndef RELICENGINE_RELICLIST_H
#define RELICENGINE_RELICLIST_H

#include "Shared.h"

typedef struct RelicListElement{

    void *data;

    struct RelicListElement *next;
    struct RelicListElement *last;

}RelicListElement;


typedef struct RelicList{

    int count;

    RelicListElement *first;
    RelicListElement *last;

}RelicList;

RelicList* RelicListCreateList();
void RelicListFreeList(RelicList*list);
void RelicListAddElement(RelicList *self,void*data);
void RelicListRemoveElement(RelicList *self,void*data);
void*RelicListGetElement(RelicList *self,int i);
void*RelicListFindElement(RelicList *self,void*data);
void **RelicListGetElement_p(RelicList *self, int i);

#endif //RELICENGINE_RELICLIST_H

#ifndef RELIC_RENDERER_H
#define RELIC_RENDERER_H

#include "Shared.h" 
//#include "RelicTextureManager.h"
//#include "Vector2int.h"
#include "Shape.h"
#include "RelicMapManager.h"

//struct Vector2int_t;
struct RelicTexture;

struct Sprite;

typedef struct AnimFrame{
    int GID;
    struct Sprite *sprite;
    time_t time;
}AnimFrame;

typedef struct Sprite
{
	struct RelicTexture *texture;
	int x;
	int y;
	int width;
	int height;

	AnimFrame *frames;
	int frameCount;
	int currentFrame;
	long long int lastTime;

	bool hasCollision;
	Shape collision;
} Sprite_t;

typedef struct Shader {
	int a;
} Shader_t;


typedef struct RelicRenderer
{
    SDL_Window *window;

	SDL_Renderer *sdlRenderer;
	SDL_Texture *debugTarget;
	SDL_Texture *lightingMap;
    SDL_Texture *overlayTarget;
	SDL_Texture *guiTarget;

	int width;
	int height;


}RelicRenderer;

void RenderSprite(struct RelicRenderer *self,Sprite_t *i, Shader_t *s, Vector2int_t pos);
void RenderSprite_anim(struct RelicRenderer *self,Sprite_t *i, Shader_t *s, Vector2int_t pos, bool noAnim,bool stopLastFrame);
void RenderSprite_double(struct RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2double_t pos, bool noAnim,bool stopLastFrame);
void Present(struct RelicRenderer *self);
void Clear(struct RelicRenderer *self);


void RenderDebugRect(Vector2int_t a, Vector2int_t b, Uint32 color);
void RenderDebugShapeAtPos(Shape shape, Vector2int_t pos, Uint32 color);
void RenderDebugPoint(Vector2int_t a, Uint32 color);

void RenderSprite_lighting(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2int_t pos,SDL_Color color, bool noAnim);
void RenderSprite_lighting_double(RelicRenderer *self, Sprite_t *i, Shader_t *s, Vector2double_t pos,SDL_Color color, bool noAnim);

void SetDefaultLightLevel(int lightLevel);
void SetOverlayLightLevel(int lightLevel);

void RenderGUIText(SDL_Texture *target,Vector2int_t pos, TTF_Font *font, char *text, SDL_Color color);
SDL_Rect RenderGuiTextCentered(SDL_Texture *target,  Vector2int_t pos, TTF_Font *font, char *text, SDL_Color color);
SDL_Rect GetRenderGUITextSize(TTF_Font *font, char *text);
void RenderGUISprite(SDL_Texture *target,Sprite_t *i, Shader_t *s, Vector2int_t pos,SDL_Color color, bool noAnim);
void RenderGUIRect(SDL_Texture *target,Vector2int_t a, int width, int height, SDL_Color color);

void RenderTextureToTarget(SDL_Texture *texture, SDL_Texture *target);
void ClearTexture(SDL_Texture *texture, SDL_Color color);

RelicRenderer *g_renderer;

RelicRenderer* RelicCreateRenderer();
void FreeRenderer();


#endif // !RELIC_RENDERER_H




//
// Created by dpete on 11/5/2018.
//

#ifndef RELICENGINE_RELICENTITY_H
#define RELICENGINE_RELICENTITY_H

#include "RelicEngine/RelicPhysics.h"
#include "Shared.h"

typedef struct ValueKey ValueKey;


typedef struct RelicEntity {
    int id;
    char name[20];
    char type[20];
    Vector2double_t pos;

    bool doNotDestroyOnLoad;

    void *data;

    bool hasPhysics;
    Physics physics;

    void (*Start)(struct RelicEntity *self);

    void (*Update)(struct RelicEntity *self);
    void (*LateUpdate)(struct RelicEntity *self);

    void (*Hurt)(struct RelicEntity *self, int amount);

    void (*Draw)(struct RelicEntity *self);

    void (*Free)(struct RelicEntity *self);
} RelicEntity_t;

void SetupEntity(RelicEntity_t *entity, char*type,char*name, Vector2double_t pos, bool doNotDestroyOnLoad);
void SetupPhysics(RelicEntity_t *entity, bool isStatic, Shape *shape);

typedef void (*Callback)(struct RelicEntity *self);
typedef void (*HurtCallback)(struct RelicEntity *self, int amount);
void SetupFunctions(RelicEntity_t *entity, Callback Start, Callback Update, Callback LateUpdate,HurtCallback Hurt, Callback Draw, Callback Free);





#endif //RELICENGINE_RELICENTITY_H

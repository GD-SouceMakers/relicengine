//
// Created by dpete on 11/6/2018.
//

#ifndef RELICENGINE_RELICREGISTY_H
#define RELICENGINE_RELICREGISTY_H

#include "Vector2int.h"
#include "RelicEntity.h"



typedef struct ValueKey {
    char value[1024];
    char key[32];
    struct ValueKey *next;
} ValueKey;

typedef RelicEntity_t*(*EntityCreateCall)(Vector2double_t pos, char*type,char*name, struct ValueKey *data);
//typedef void (*SignalHandler)(int signum);


typedef struct EntityType {
    char name[20];

    EntityCreateCall createCall;

    struct EntityType *next;
} EntityType;

RelicEntity_t *CreateEntityOfType(char*type,Vector2double_t pos, char *name, struct ValueKey *data);

void RegisterEntity(const char type[], EntityCreateCall createCall);

void RegisterDefaults();

void FreeRegistry();



int GetIntValueKey(struct ValueKey *data, const char *key);
double GetDoubleValueKey(struct ValueKey *data, const char *key);
char* GetStringValueKey(struct ValueKey *data, const char *key);
SDL_Color GetColorValueKey(struct ValueKey *data, const char *key);
Vector2double_t GetVector2doubleValueKey(ValueKey *data, const char *key);

#endif //RELICENGINE_RELICREGISTY_H

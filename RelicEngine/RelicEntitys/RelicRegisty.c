#include <RelicEngine/RelicEntitys/RelicPlayer.h>
#include "Vector2int.h"
#include "RelicRegisty.h"
#include "RelicCamera.h"
#include "EmptyEntity.h"
#include "MapChange.h"
#include "LightSource.h"
#include "Spider.h"
#include "BasicCollider.h"

static EntityType *entityRegistyFirst = NULL;
static EntityType *entityRegistyLast = NULL;

RelicEntity_t *CreateEntityOfType(char *type, Vector2double_t pos, char *name, struct ValueKey *data) {

    EntityType *current = entityRegistyFirst;

    while (strcmp(current->name, type)) {
        current = current->next;
        if (current == NULL) {
            return NULL;
        }
    }


    return current->createCall(pos, current->name, name, data);
}

void RegisterEntity(const char type[], EntityCreateCall createCall) {
    EntityType *next = malloc(sizeof(EntityType));
    strcpy(next->name, type);
    next->createCall = createCall;

    if (entityRegistyFirst == NULL) {
        entityRegistyFirst = next;
    } else {
        entityRegistyLast->next = next;
    }
    entityRegistyLast = next;
    entityRegistyLast->next = NULL;
}


void RegisterDefaults() {
    RegisterCamera();
    RegisterEmpty();
    RegisterPlayer();
    RegisterMapChange();
    RegisterLightSource();
    RegisterSpider();
    RegisterBasicCollision();
};

void FreeRegistry() {

    EntityType *temp = entityRegistyFirst;
    while (temp != NULL) {
        EntityType *next = temp->next;
        free(temp);
        temp = next;
    }

}




int GetIntValueKey(ValueKey *data, const char *key){
    ValueKey *next = data;
    while (next != NULL) {

        if (strcmp(next->key, key) == 0) {
            return strtol(next->value,NULL,0);
        }
        next = next->next;
    }
}

double GetDoubleValueKey(ValueKey *data, const char *key){
    ValueKey *next = data;
    while (next != NULL) {

        if (strcmp(next->key, key) == 0) {
            return strtod(next->value,NULL);
        }
        next = next->next;
    }
}

char* GetStringValueKey(ValueKey *data, const char *key){
    ValueKey *next = data;
    while (next != NULL) {

        if (strcmp(next->key, key) == 0) {

            char*res = malloc(sizeof(char)*(strlen(next->value)+1));
            strcpy(res,next->value);
            return res;
        }
        next = next->next;
    }
}

SDL_Color GetColorValueKey(ValueKey *data, const char *key){
    ValueKey *next = data;
    SDL_Color color;
    while (next != NULL) {

        if (strcmp(next->key, key) == 0) {
            unsigned long code = strtoul(next->value,NULL,16);
            color.a = code >> 24;
            color.r = code >> 16 - (color.a << 8);
            color.g = code >> 8 - (color.a << 16)- (color.r << 8);
            color.b = code >> 0 - (color.a << 24)- (color.r << 16) -(color.g << 8);
            return color;
        }
        next = next->next;
    }
}

Vector2double_t GetVector2doubleValueKey(ValueKey *data, const char *key){
    ValueKey *next = data;
    SDL_Color color;
    while (next != NULL) {

        if (strcmp(next->key, key) == 0) {
            Vector2double_t vector;
            sscanf(next->value,"%lf,%lf",  &vector.x, &vector.y);
            return vector;
        }
        next = next->next;
    }
}
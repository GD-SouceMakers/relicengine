//
// Created by dpete on 11/6/2018.
//

#include <RelicEngine/RelicMap/RelicMap.h>
#include "RelicRegisty.h"

RelicEntity_t* CreateEmpty(Vector2double_t pos,char*type,char*name, ValueKey *data){
    struct RelicEntity *res =malloc(sizeof(struct RelicEntity));
    SetupEntity(res,type,name,pos,false);


    return res;
}

void RegisterEmpty(){
    RegisterEntity("info_player_start",&CreateEmpty);
    RegisterEntity("empty",&CreateEmpty);
    RegisterEntity("marker",&CreateEmpty);
}
//
// Created by dpete on 11/5/2018.
//

#include "RelicEntity.h"

void SetupEntity(RelicEntity_t *entity, char *type, char *name, Vector2double_t pos, bool doNotDestroyOnLoad) {
    strcpy(entity->type, type);
    strcpy(entity->name, name);
    entity->data = NULL;
    entity->pos = pos;

    entity->doNotDestroyOnLoad = doNotDestroyOnLoad;

    entity->hasPhysics = false;

    SetupFunctions(entity,NULL,NULL,NULL,NULL,NULL,NULL);
}


void SetupPhysics(RelicEntity_t *entity, bool isStatic, Shape *shape) {
    entity->hasPhysics = true;

    entity->physics.isStatic = isStatic;

    entity->physics.shape = shape;
    entity->physics.pos = &entity->pos;
    entity->physics.collision = NULL;
}


void SetupFunctions(RelicEntity_t *entity, Callback Start, Callback Update, Callback LateUpdate,HurtCallback Hurt, Callback Draw,
                    Callback Free) {
    entity->Update = Update;
    entity->LateUpdate = LateUpdate;
    entity->Draw = Draw;
    entity->Start = Start;
    entity->Hurt = Hurt;
    entity->Free = Free;
}
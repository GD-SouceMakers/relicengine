#include <Program.h>
#include "RelicPlayer.h"

#include "RelicTimer.h"
#include "Input.h"
#include "RelicTextureManager.h"

RelicPlayer_t *g_player;

int GetAction(RelicPlayer_t *self, char *name) {
    for (int i = 0; i < self->characterData->actionCount; ++i) {
        if (strcmp(self->characterData->actions[i].name, name) == 0) {
            return i;
        }
    }
    SDL_LogError(RELICLOG_CATEGORY_DEBUG, "Can't find the action: %s", name);
    return -1;
}


void Start(RelicEntity_t *self) {

}

void Update(RelicEntity_t *self) {
    RelicPlayer_t *player = self->data;


    if (g_relicapp->state == IN_GAME && player->alive && ((globalInput.left != 0) || (globalInput.forward != 0))) {
        Vector2double_t vel = {0, 0};
        if (globalInput.left != 0) {
            //self->pos.x += globalInput.left * self->speed * deltaTime;
            vel.x = globalInput.left;
            player->movement = globalInput.left > 0 ? RELICPLAYER_MOVEMENT_RIGHT : RELICPLAYER_MOVEMENT_LEFT;
        }
        if (globalInput.forward != 0) {
            //self->pos.y += globalInput.forward * self->speed * deltaTime;
            vel.y = globalInput.forward;
            player->movement = globalInput.forward > 0 ? RELICPLAYER_MOVEMENT_DOWN : RELICPLAYER_MOVEMENT_UP;
        }
        Vector2double_Normalize(&vel);
        Vector2double_Multplydouble(&vel, player->speed);

        self->physics.velocity = vel;

    } else if (!player->alive) {
        player->movement = RELICPLAYER_MOVEMENT_DIE;
    } else {
        player->movement = 0;
        self->physics.velocity = (Vector2double_t) {0, 0};
    }


    //SDL_LogError(RELICLOG_CATEGORY_DEBUG, "%lf %lf",self->physics->velocity.x,self->physics->velocity.y);
}

void Draw(RelicEntity_t *self) {
    RelicPlayer_t *player = ((RelicPlayer_t *) self->data);
    bool singleTime = false;
    char action[20];
    switch (player->movement) {
        case RELICPLAYER_MOVEMENT_UP:
            strcpy(action, "forward");
            break;
        case RELICPLAYER_MOVEMENT_DOWN:
            strcpy(action, "back");
            break;
        case RELICPLAYER_MOVEMENT_LEFT:
            strcpy(action, "left");
            break;
        case RELICPLAYER_MOVEMENT_RIGHT:
            strcpy(action, "right");
            break;
        case RELICPLAYER_MOVEMENT_DIE:
            strcpy(action, "die");
            singleTime = true;
            break;
        default:
            strcpy(action, "back");
    }

    int id = player->characterData->actions[GetAction(player, action)].strite;
    Sprite_t *sprite = &player->characterData->characterSheat->sprites[id];
    Vector2double_t drawPos = self->pos;
    Vector2double_Substract(&drawPos, player->offset);
    RenderSprite_double(g_renderer, sprite, NULL, drawPos,
                        player->movement == 0,singleTime);


    sprite = &player->light_sheet->sprites[0];
    Vector2double_t shadowPos = self->pos;
    Vector2double_Substract(&shadowPos, (Vector2double_t) {.x=sprite->width / 2, .y=sprite->height / 2});
    RenderSprite_lighting_double(g_renderer, sprite, NULL, shadowPos, (SDL_Color) {255, 255, 255, 255}, true);

}

void Free(RelicEntity_t *self) {
    RelicPlayer_t *playerData = self->data;

    free(playerData->characterData->actions);
    free(playerData->characterData);
}

void HurtPlayer(RelicEntity_t *self, int amount) {
    RelicPlayer_t *data = ((RelicPlayer_t *) self->data);

    if (data->hp - amount <= 0) {
        //DEAD
        data->hp = 0;
        data->alive = false;
    } else {
        data->hp -= amount;
    }
    //SDL_LogError(RELICLOG_CATEGORY_DEBUG, "%d", data->hp);
}

RelicEntity_t *CreatePlayer(Vector2double_t pos, char *type, char *name, ValueKey *data) {
    struct RelicEntity *res = malloc(sizeof(struct RelicEntity));

    SetupEntity(res, type, name, pos, true);

    SetupFunctions(res, &Start, &Update, NULL, &HurtPlayer, &Draw, &Free);


    res->data = malloc(sizeof(RelicPlayer_t));
    RelicPlayer_t *playerData = res->data;
    playerData->alive = true;
    g_player = playerData;

    playerData->characterData = ParseCharacterFile("player");
    playerData->hp = GetIntValueKey(playerData->characterData->propertyes, "hp");
    playerData->max_hp = playerData->hp;
    playerData->speed = GetDoubleValueKey(playerData->characterData->propertyes, "speed");
    FreeValuKeyList(playerData->characterData->propertyes);

    //FIXME: This modifies the original vertexes. Should be a Copy
    Shape *shape = ShapeDuplicate(playerData->characterData->characterSheat->sprites[0].collision);
    playerData->offset = ShapeCenter(*shape);
    TranslateShape(shape, (Vector2double_t) {.x=-playerData->offset.x, .y=-playerData->offset.y});


    SetupPhysics(res, false, shape);

    playerData->light_sheet = GetTexture(g_textureManager, "light_03");


    return res;
}

void RegisterPlayer() {
    RegisterEntity("player", &CreatePlayer);
}


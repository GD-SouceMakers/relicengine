//
// Created by Peter on 11/22/2018.
//

#include <stdbool.h>
#include <RelicEngine/Vector2int.h>
#include "BasicCollider.h"
#include "RelicEntity.h"
#include "RelicRegisty.h"

void FreeBasicCollision(RelicEntity_t *self){

}

RelicEntity_t* CreateBasicCollision(Vector2double_t pos,char*type,char*name, ValueKey *data){
    struct RelicEntity *res =malloc(sizeof(struct RelicEntity));
    SetupEntity(res,type,name,pos,false);

    int pointCount = GetIntValueKey(data,"PointCount");

    Shape *shape = malloc(sizeof(Shape));
    shape->vertices = malloc(sizeof(Vector2double_t)*pointCount);
    shape->vertCount = pointCount;

    for (int i = 0; i < pointCount; ++i) {
        char name[8];
        sprintf(name, "Point%d",i);

        shape->vertices[i] = GetVector2doubleValueKey(data,name);
    }

    SetupPhysics(res,true,shape);

    return res;
}

void RegisterBasicCollision(){
    RegisterEntity("collision",&CreateBasicCollision);
}
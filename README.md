# Relic Engine and Grimphobia  

## Relic engine  
Relic engine is uni. project where I attempt to make a 2d zelda like game engine I C.
The engine is a bit wired name for it because it isn't going to be a truly generic system, it will only have the
functionality that Grimphobia will require 

### Engine features  
- Map load from file with arbitrary sizes
- Load Entities from the map file
- Load texture sheets from files with arbitrary sizes
- Have the camera follow the player
- Have enemies that follow simple paths

### Screenshots  

![Screenshot](web/images/RelicEngine_2018-10-28_16-02-49.png)

![Screenshot](web/images/2018-10-28_16-03-55.gif)

![Screenshot](web/images/2018-10-29_13-40-22.gif)

## Grimphobia  
Grimphobia is the game that I make the engine for. It is a simple labyrinth game.
 where you have to collect keys to advance and avoid enemies that move along simple paths.

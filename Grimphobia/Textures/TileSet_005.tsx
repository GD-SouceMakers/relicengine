<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="TileSet_005" tilewidth="32" tileheight="32" tilecount="384" columns="24">
 <image source="TileSet_005.png" trans="4b637f" width="768" height="512"/>
 <terraintypes>
  <terrain name="Wall_Dirt" tile="85"/>
  <terrain name="Floor_01" tile="201"/>
 </terraintypes>
 <tile id="1">
  <animation>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="24" duration="200"/>
  </animation>
 </tile>
 <tile id="3">
  <animation>
   <frame tileid="3" duration="200"/>
   <frame tileid="5" duration="200"/>
   <frame tileid="7" duration="200"/>
  </animation>
 </tile>
 <tile id="4">
  <animation>
   <frame tileid="4" duration="200"/>
   <frame tileid="6" duration="200"/>
   <frame tileid="8" duration="200"/>
  </animation>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="10" height="32"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index">
   <object id="1" x="24" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="24" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="27">
  <animation>
   <frame tileid="27" duration="200"/>
   <frame tileid="29" duration="200"/>
   <frame tileid="31" duration="200"/>
  </animation>
 </tile>
 <tile id="28">
  <animation>
   <frame tileid="28" duration="200"/>
   <frame tileid="30" duration="200"/>
   <frame tileid="32" duration="200"/>
  </animation>
 </tile>
 <tile id="36">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="38">
  <objectgroup draworder="index">
   <object id="1" x="24" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="39">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="41">
  <objectgroup draworder="index">
   <object id="1" x="24" y="0" width="8" height="32"/>
  </objectgroup>
 </tile>
 <tile id="48">
  <animation>
   <frame tileid="48" duration="200"/>
   <frame tileid="51" duration="200"/>
   <frame tileid="54" duration="200"/>
  </animation>
 </tile>
 <tile id="49">
  <animation>
   <frame tileid="49" duration="200"/>
   <frame tileid="52" duration="200"/>
   <frame tileid="55" duration="200"/>
  </animation>
 </tile>
 <tile id="50">
  <animation>
   <frame tileid="50" duration="200"/>
   <frame tileid="53" duration="200"/>
   <frame tileid="56" duration="200"/>
  </animation>
 </tile>
 <tile id="60">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0">
    <polygon points="0,0 0,32 32,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index">
   <object id="1" x="0" y="24" width="32" height="8"/>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index">
   <object id="3" x="0" y="32">
    <polygon points="0,0 32,-32 32,0 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="63">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 0,32 32,32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="64">
  <objectgroup draworder="index">
   <object id="1" x="0" y="24" width="32" height="8"/>
  </objectgroup>
 </tile>
 <tile id="65">
  <objectgroup draworder="index">
   <object id="2" x="0" y="32">
    <polygon points="0,0 32,-32 32,0 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="72">
  <animation>
   <frame tileid="72" duration="200"/>
   <frame tileid="75" duration="200"/>
   <frame tileid="78" duration="200"/>
  </animation>
 </tile>
 <tile id="73">
  <animation>
   <frame tileid="73" duration="200"/>
   <frame tileid="76" duration="200"/>
   <frame tileid="79" duration="200"/>
  </animation>
 </tile>
 <tile id="74">
  <animation>
   <frame tileid="74" duration="200"/>
   <frame tileid="77" duration="200"/>
   <frame tileid="80" duration="200"/>
  </animation>
 </tile>
 <tile id="86" terrain="0,0,0,">
  <objectgroup draworder="index">
   <object id="4" x="0" y="32">
    <polygon points="0,0 0,-32 32,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="87" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="-0.0909091" y="-0.181818" width="32.1818" height="7.72727"/>
  </objectgroup>
 </tile>
 <tile id="88" terrain="0,0,,0">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0">
    <polygon points="0,0 32,0 32,32 0,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="89" terrain="0,,,"/>
 <tile id="90" terrain=",0,,"/>
 <tile id="96">
  <animation>
   <frame tileid="96" duration="200"/>
   <frame tileid="99" duration="200"/>
   <frame tileid="102" duration="200"/>
  </animation>
 </tile>
 <tile id="97">
  <animation>
   <frame tileid="97" duration="200"/>
   <frame tileid="100" duration="200"/>
   <frame tileid="103" duration="200"/>
  </animation>
 </tile>
 <tile id="98">
  <animation>
   <frame tileid="98" duration="200"/>
   <frame tileid="101" duration="200"/>
   <frame tileid="104" duration="200"/>
  </animation>
 </tile>
 <tile id="110" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="4" height="32"/>
  </objectgroup>
 </tile>
 <tile id="112" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="28" y="0" width="4" height="32"/>
  </objectgroup>
 </tile>
 <tile id="113" terrain=",,0,"/>
 <tile id="114" terrain=",,,0"/>
 <tile id="134" terrain="0,,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="4" height="32"/>
  </objectgroup>
 </tile>
 <tile id="135" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="4" height="32"/>
  </objectgroup>
 </tile>
 <tile id="136" terrain=",0,0,0">
  <objectgroup draworder="index">
   <object id="1" x="28" y="0" width="4" height="32"/>
  </objectgroup>
 </tile>
 <tile id="179" terrain="1,1,,1"/>
 <tile id="180" terrain=",1,1,1"/>
 <tile id="181" terrain="1,1,1,"/>
 <tile id="201" terrain="1,1,1,1"/>
 <tile id="202" terrain="1,,1,1"/>
 <tile id="203" terrain=",,,1"/>
 <tile id="204" terrain=",,1,1"/>
 <tile id="205" terrain=",,1,"/>
 <tile id="227" terrain=",1,,1"/>
 <tile id="229" terrain="1,,1,"/>
 <tile id="251" terrain=",1,,"/>
 <tile id="252" terrain="1,1,,"/>
 <tile id="253" terrain="1,,,"/>
</tileset>

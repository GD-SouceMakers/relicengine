<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="character_test_jeff" tilewidth="64" tileheight="64" tilecount="273" columns="13">
 <image source="character_test_jeff.png" trans="4b637f" width="832" height="1344"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="20" y="54" width="24" height="10"/>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="200"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="4" duration="200"/>
   <frame tileid="5" duration="200"/>
   <frame tileid="6" duration="200"/>
  </animation>
 </tile>
 <tile id="13">
  <animation>
   <frame tileid="13" duration="200"/>
   <frame tileid="14" duration="200"/>
   <frame tileid="15" duration="200"/>
   <frame tileid="16" duration="200"/>
   <frame tileid="17" duration="200"/>
   <frame tileid="18" duration="200"/>
   <frame tileid="19" duration="200"/>
  </animation>
 </tile>
 <tile id="26">
  <animation>
   <frame tileid="26" duration="200"/>
   <frame tileid="27" duration="200"/>
   <frame tileid="28" duration="200"/>
   <frame tileid="29" duration="200"/>
   <frame tileid="30" duration="200"/>
   <frame tileid="31" duration="200"/>
   <frame tileid="32" duration="200"/>
  </animation>
 </tile>
 <tile id="39">
  <animation>
   <frame tileid="39" duration="200"/>
   <frame tileid="40" duration="200"/>
   <frame tileid="41" duration="200"/>
   <frame tileid="42" duration="200"/>
   <frame tileid="43" duration="200"/>
   <frame tileid="44" duration="200"/>
   <frame tileid="45" duration="200"/>
  </animation>
 </tile>
 <tile id="104">
  <animation>
   <frame tileid="104" duration="200"/>
   <frame tileid="105" duration="200"/>
   <frame tileid="106" duration="200"/>
   <frame tileid="107" duration="200"/>
   <frame tileid="108" duration="200"/>
   <frame tileid="109" duration="200"/>
   <frame tileid="110" duration="200"/>
   <frame tileid="111" duration="200"/>
   <frame tileid="112" duration="200"/>
  </animation>
 </tile>
 <tile id="117">
  <animation>
   <frame tileid="117" duration="200"/>
   <frame tileid="118" duration="200"/>
   <frame tileid="119" duration="200"/>
   <frame tileid="120" duration="200"/>
   <frame tileid="121" duration="200"/>
   <frame tileid="122" duration="200"/>
   <frame tileid="123" duration="200"/>
   <frame tileid="124" duration="200"/>
   <frame tileid="125" duration="200"/>
  </animation>
 </tile>
 <tile id="130">
  <animation>
   <frame tileid="130" duration="200"/>
   <frame tileid="131" duration="200"/>
   <frame tileid="132" duration="200"/>
   <frame tileid="133" duration="200"/>
   <frame tileid="134" duration="200"/>
   <frame tileid="135" duration="200"/>
   <frame tileid="136" duration="200"/>
   <frame tileid="137" duration="200"/>
   <frame tileid="138" duration="200"/>
  </animation>
 </tile>
 <tile id="143">
  <animation>
   <frame tileid="143" duration="200"/>
   <frame tileid="144" duration="200"/>
   <frame tileid="145" duration="200"/>
   <frame tileid="146" duration="200"/>
   <frame tileid="147" duration="200"/>
   <frame tileid="148" duration="200"/>
   <frame tileid="149" duration="200"/>
   <frame tileid="150" duration="200"/>
   <frame tileid="151" duration="200"/>
  </animation>
 </tile>
 <tile id="260">
  <animation>
   <frame tileid="260" duration="200"/>
   <frame tileid="261" duration="200"/>
   <frame tileid="262" duration="200"/>
   <frame tileid="263" duration="200"/>
   <frame tileid="264" duration="200"/>
   <frame tileid="265" duration="200"/>
  </animation>
 </tile>
</tileset>

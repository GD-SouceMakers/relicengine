<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="TileSet_003" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="TileSet_003.png" trans="4b637f" width="512" height="512"/>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0.0795455" y="12.7841" width="32.1818" height="8.36364"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0.0795455" y="12.7841" width="32.1818" height="8.36364"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0.0795455" y="12.7841" width="32.1818" height="8.36364"/>
  </objectgroup>
 </tile>
 <tile id="42">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.363636" y="11.0909" width="32.5455" height="10.1818"/>
  </objectgroup>
 </tile>
 <tile id="43">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.363636" y="11.0909" width="32.5455" height="10.1818"/>
  </objectgroup>
 </tile>
 <tile id="44">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.363636" y="11.0909" width="32.5455" height="10.1818"/>
  </objectgroup>
 </tile>
 <tile id="160">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="9.625" width="8.43226" height="22.375"/>
  </objectgroup>
 </tile>
 <tile id="162">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="9.625" width="8.43226" height="22.375"/>
  </objectgroup>
 </tile>
 <tile id="176">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="0" width="8.43226" height="32"/>
  </objectgroup>
 </tile>
 <tile id="178">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="0" width="8.43226" height="32"/>
  </objectgroup>
 </tile>
 <tile id="192">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="-0.125" width="8.43226" height="25"/>
  </objectgroup>
 </tile>
 <tile id="194">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="12.9714" y="-0.125" width="8.43226" height="25"/>
  </objectgroup>
 </tile>
 <tile id="227">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="228">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="229">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="230">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="231">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="232">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="233">
  <objectgroup draworder="index">
   <object id="2" name="coll" type="collision" x="0.125" y="0" width="32" height="31.875"/>
  </objectgroup>
 </tile>
 <tile id="243">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="244">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="245">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="246">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="247">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="248">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
 <tile id="249">
  <objectgroup draworder="index">
   <object id="1" name="coll" type="collision" x="0.25" y="0" width="31.875" height="9.75"/>
  </objectgroup>
 </tile>
</tileset>

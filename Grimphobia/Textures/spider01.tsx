<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="spider01" tilewidth="64" tileheight="64" tilecount="50" columns="10">
 <image source="spider01.png" trans="4b637f" width="640" height="320"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="19.5" y="18" width="23.75" height="33.75"/>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="200"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
  </animation>
 </tile>
 <tile id="4">
  <animation>
   <frame tileid="4" duration="200"/>
   <frame tileid="5" duration="200"/>
   <frame tileid="6" duration="200"/>
   <frame tileid="7" duration="200"/>
   <frame tileid="8" duration="200"/>
   <frame tileid="9" duration="200"/>
  </animation>
 </tile>
 <tile id="10">
  <animation>
   <frame tileid="10" duration="200"/>
   <frame tileid="11" duration="200"/>
   <frame tileid="12" duration="200"/>
   <frame tileid="13" duration="200"/>
  </animation>
 </tile>
 <tile id="14">
  <animation>
   <frame tileid="15" duration="200"/>
   <frame tileid="16" duration="200"/>
   <frame tileid="17" duration="200"/>
   <frame tileid="18" duration="200"/>
   <frame tileid="19" duration="200"/>
  </animation>
 </tile>
 <tile id="20">
  <animation>
   <frame tileid="20" duration="200"/>
   <frame tileid="21" duration="200"/>
   <frame tileid="22" duration="200"/>
   <frame tileid="23" duration="200"/>
  </animation>
 </tile>
 <tile id="24">
  <animation>
   <frame tileid="25" duration="200"/>
   <frame tileid="26" duration="200"/>
   <frame tileid="27" duration="200"/>
   <frame tileid="28" duration="200"/>
   <frame tileid="29" duration="200"/>
  </animation>
 </tile>
 <tile id="30">
  <animation>
   <frame tileid="30" duration="200"/>
   <frame tileid="31" duration="200"/>
   <frame tileid="32" duration="200"/>
   <frame tileid="33" duration="200"/>
  </animation>
 </tile>
 <tile id="34">
  <animation>
   <frame tileid="35" duration="200"/>
   <frame tileid="36" duration="200"/>
   <frame tileid="37" duration="200"/>
   <frame tileid="38" duration="200"/>
   <frame tileid="39" duration="200"/>
  </animation>
 </tile>
 <tile id="40">
  <animation>
   <frame tileid="41" duration="200"/>
   <frame tileid="42" duration="200"/>
   <frame tileid="43" duration="200"/>
  </animation>
 </tile>
</tileset>

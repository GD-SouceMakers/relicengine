<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="TileSet_004" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="TileSet_004.png" trans="4b637f" width="512" height="512"/>
 <terraintypes>
  <terrain name="Farmland" tile="56"/>
  <terrain name="Wheat" tile="59"/>
  <terrain name="Green_wheat" tile="62"/>
  <terrain name="Fsrmland_2" tile="0"/>
 </terraintypes>
 <tile id="11" terrain="1,1,1,"/>
 <tile id="12" terrain="1,1,,1"/>
 <tile id="14" terrain="2,2,2,"/>
 <tile id="15" terrain="2,2,,2"/>
 <tile id="27" terrain="1,,1,1"/>
 <tile id="28" terrain=",1,1,1"/>
 <tile id="30" terrain="2,,2,2"/>
 <tile id="31" terrain=",2,2,2"/>
 <tile id="42" terrain=",,,1"/>
 <tile id="43" terrain=",,1,1"/>
 <tile id="44" terrain=",,1,"/>
 <tile id="45" terrain=",,,2"/>
 <tile id="46" terrain=",,2,2"/>
 <tile id="47" terrain=",,2,"/>
 <tile id="58" terrain=",1,,1"/>
 <tile id="59" terrain="1,1,1,1"/>
 <tile id="60" terrain="1,,1,"/>
 <tile id="61" terrain=",2,,2"/>
 <tile id="62" terrain="2,2,2,2"/>
 <tile id="63" terrain="2,,2,"/>
 <tile id="74" terrain=",1,,"/>
 <tile id="75" terrain="1,1,,"/>
 <tile id="76" terrain="1,,,"/>
 <tile id="77" terrain=",2,,"/>
 <tile id="78" terrain="2,2,,"/>
 <tile id="79" terrain="2,,,"/>
 <tile id="93" terrain=",,,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="13.625" y="32">
    <polygon points="0,0 18.375,-22.75 18.25,0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="94" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.125" y="11.875" width="32.125" height="20.125"/>
  </objectgroup>
 </tile>
 <tile id="95" terrain=",,0,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0.375" y="12.875">
    <polygon points="0,0 16.875,19 -0.375,19.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="109" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="15" y="-0.25" width="16.875" height="32.25"/>
  </objectgroup>
 </tile>
 <tile id="110" terrain="0,0,0,0"/>
 <tile id="111" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0" y="-0.25" width="17.25" height="32.125"/>
  </objectgroup>
 </tile>
 <tile id="125" terrain=",0,,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="16.25" y="0.125">
    <polygon points="0,0 15.625,20.875 15.25,-0.125"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="126" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.125" y="0.25" width="32.25" height="19.875"/>
  </objectgroup>
 </tile>
 <tile id="127" terrain="0,,,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0.125" y="17.5">
    <polygon points="0,0 16.625,-17.375 -0.125,-17.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="159" terrain="3,3,3,3"/>
 <tile id="190" terrain="0,0,0,">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="15.125" y="31.5">
    <polygon points="0,0 16.875,-15.25 16.875,-31.625 -15.25,-31.625 -15.125,0.75"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="191" terrain="0,0,,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="0" y="13.375">
    <polygon points="0,0 17,18.875 31.625,18.375 31.625,-13.5 0,-13.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="206" terrain="0,,0,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="14.25" y="0.25">
    <polygon points="0,0 17.75,17.125 17.75,31.25 -14.125,31.375 -14.25,-0.25"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="207" terrain=",0,0,0">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="-0.125" y="17.5">
    <polygon points="0,0 16.375,-17.25 31.75,-17.25 32.125,14.5 0.25,14.375"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="216">
  <animation>
   <frame tileid="216" duration="180"/>
   <frame tileid="218" duration="180"/>
  </animation>
 </tile>
 <tile id="217">
  <animation>
   <frame tileid="217" duration="180"/>
   <frame tileid="219" duration="180"/>
  </animation>
 </tile>
 <tile id="222">
  <animation>
   <frame tileid="222" duration="240"/>
   <frame tileid="223" duration="240"/>
  </animation>
 </tile>
 <tile id="232">
  <animation>
   <frame tileid="232" duration="180"/>
   <frame tileid="234" duration="180"/>
  </animation>
 </tile>
 <tile id="233">
  <animation>
   <frame tileid="233" duration="180"/>
   <frame tileid="235" duration="180"/>
  </animation>
 </tile>
 <tile id="248">
  <animation>
   <frame tileid="248" duration="180"/>
   <frame tileid="250" duration="180"/>
  </animation>
 </tile>
 <tile id="249">
  <animation>
   <frame tileid="249" duration="180"/>
   <frame tileid="251" duration="180"/>
  </animation>
 </tile>
 <tile id="255">
  <objectgroup draworder="index">
   <object id="1" type="collision" x="11.125" y="7.625" width="10" height="28.75"/>
  </objectgroup>
 </tile>
</tileset>

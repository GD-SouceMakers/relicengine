# MOVED TO THE WIKI [rmf](https://gitlab.com/GD-SouceMakers/relicengine/wikis/Engine-basics/Relic%20map%20format) [rsf](https://gitlab.com/GD-SouceMakers/relicengine/wikis/Engine-basics/Relic%20sheet%20format) [rpf](https://gitlab.com/GD-SouceMakers/relicengine/wikis/Engine-basics/Relic%20player%20format)

The file formats used by this program are ``.rmf`` and ``.rsf`` from maps and tile sheets respectively.
Both are strongly ordered in their layout and are designed to make it easy to read them form low level languages like c/c++.

##Relic Map Format (1.0.0)  
It supports both tile based maps and Entity based ones and mixed maps as well.
The files format:
```
RMF_1.0.0
PLACEHOLDER_NAME
100,100
32,32
/SC-1
/S-TileSet_001,1
/LC-8
/L-Ground
81,81,81,81,81,8
[...]
/EC-2
/E-Entities,2
1-401:915-start:info_player_start
2-276:800-name:test
```
Line by line:
1.  FileFormat and version specification
2.  Name of the map max 20 chars
3.  the size of the map on the x,y dimension (separator: ',')
4.  the size of the tiles used in the map (separator ',')

From here we only specify that some parts should be before others but no strict order  
- /S lines are specifying tile sheets:  
the fist should be /SC  
  - ```/SC-```+ the count of the tile sheets used (this is so the program can make the necessary memory allocations)  
  - ```/S-``` + the name of the tile sheet and the staring index in the the global tile ids (separator: ',')  

- /L lines are for defining tile layers  
the fist should be /LC  
  - ```/LC-```+ the count of the tile layers used (this is so the program can make the necessary memory allocations)  
  - ```/L-``` + the name of the tile layer name and the next lines are the ',' separated tile IDs on that map  

- /E lines are to defining the entities  
the fist should be /EC  
  - ```/EC-```+ the count of the Entities in all layers (this is so the program can make the necessary memory allocations)  
  - ```/E-``` + the name of the entity layer (only for grouping) the following lines are specifying the entities:  
    ``1-401:915-start:info_player_start``
    ``ID+Posx:Posy-NAME:TYPE``

##Relic Sheet Format(1.0.0)
It describes the  layout of the tile sets, it only supports grid layout
The file format:
```
RSF_1.0.0
TileSet_001
TileSet_001.png
0
16,16
32,32
211-5,211:200,212:200,213:200,214:200,215:200
213-5,213:200,214:200,215:200,211:200,212:200
227-5,227:200,228:200,229:200,230:200,231:200
229-5,229:200,230:200,231:200,227:200,228:200
243-5,243:200,244:200,245:200,246:200,247:200
245-5,245:200,246:200,247:200,243:200,244:200
```
Line by line:
1.  FileFormat and version specification
2.  Name of the map     (max 20 chars)
3.  Texture file name   (max 20 chars)
4.  The starting index inside the sheet
5.  the size of the map on the x,y dimension (separator: ',')
6.  the size of the tiles used in the map (separator ',')

The next lines are describing the animations of each tile.
Not every tile ID has to have an animation, and they don't have to be in any specific order.

``211-5,211:200,212:200,213:200,214:200,215:200``   
``TILEID-N,FRAMEID:TIME,[...]``


Symbol | meaning 
--- | --- 
``TILEID``  |   the id of the tile that will be animated 
``N``       |   the number of the animation frames 
``FRAMEID`` |   the ID of the tile that will be used as a animation frame 
``TIME``    |   the amount of time that frame will be shown for 

``FRAMEID:TIME`` should be repeated ``N`` times with ',' as a separator


##Relic Player Format(0.0.0)
This file describes the basics of the player (currently only the graphic)  
The Format:
```
rpf_0.0.0
name
character_test_jeff
/AC-5
/A-forward:0
/A-back:1
/A-left:2
/A-right:3
/A-die:4
```
Line by line:
1.  Describes the format and the version
2.  The name of the character
3.  The tile sheet file that should be used to display the character

From here we only specify that some parts should be before others but no strict order  
/A lines are specifying actions:  
the fist should be ``/AC``  
- ```/AC-```+ the count of the actions used (this is so the program can make the necessary memory allocations)  
- ```/A-``` + the name of the action and the id of the sprite used for it (separator: ':') 
 